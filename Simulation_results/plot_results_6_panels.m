figure;

%% plotting the result of the Simulink simulation
blocaux = extract_blocaux(mpm.mpc);
index_20kV_nodes = find(blocaux.bus(:,10)==20);
index_90kV_nodes = find(blocaux.bus(:,10)==90);
index_225kV_nodes = find(blocaux.bus(:,10)==225);

%% curtailment Blocaux in MW
% subplot(2,3,5)
% plot(curtailment_blocaux.Time/60,curtailment_blocaux.Data)
% title('Power Curtailment')
% grid;
% xlabel('Time [min]')
% ylabel('Power Curtailment [MW]')
% xlim([0,1600/60])
% ylim([0 35])
% xticks([0,10,15,20,25])

%% curtailment Blocaux in Percent
subplot(2,3,5)
plot(curtailment_blocaux.Time/60, 100 * curtailment_blocaux.Data ./ blocaux.gen(:,9)')
title('Power Curtailment')
grid;
xlabel('Time [min]')
ylabel('Power Curtailment [%]')
xlim([0,1600/60])
ylim([0 110])
xticks([0,5,10,15,20,25])

%% wind
% wind power generation
subplot(2,3,3)
plot(wind_power.Time/60, wind_power.Data(:,1)/blocaux.gen(1,9)*100)
hold on;
% Percentage of wind power we can use
subplot(2,3,3);
sum_injected_p = sum(p_Blocaux.Data,2);
sum_curtailed_p = sum(curtailment_blocaux.Data,2);
sum_possible_p = sum_injected_p + sum_curtailed_p;

percentage_wind_power = sum_injected_p ./ sum_possible_p;

plot(wind_power.Time/60, percentage_wind_power .* wind_power.Data(:,1)/blocaux.gen(1,9)*100);

ylim([0, 100])
grid;
title('Wind Power [%]')
xlabel('Time [min]')
ylabel('Wind Power [%]')
xlim([0,1600/60])
xticks([0,5,10,15,20,25])
lgd = legend('Available','Used');
lgd.Location = 'northwest';
%% line usage
subplot(2,3,2)
lines = find(blocaux.branch(:,5)~=0); 
plot(line_usage_Blocaux.Time/60, line_usage_Blocaux.Data(:,lines))
title('Line Usage')
grid;
xlabel('Time [min]')
ylabel('Line Usage [p.u.]')
xlim([0,1600/60])
ylim([0,1.2])
xticks([0,5,10,15,20,25])
%% tap setpoints
subplot(2,3,6)
plot(tap_Blocaux_quantitized.Time/60,tap_Blocaux_quantitized.Data)
% plot(tap_Blocaux_pre_quantization.Time,tap_Blocaux_pre_quantization.Data)
title('Tap Ratio')
grid;
xlabel('Time [min]')
ylabel('Tap Ratio')
xlim([0,1600/60])
ylim([0.85 1.15])
xticks([0,5,10,15,20,25])
%% voltages in the Blocaux area
subplot(2,3,1);
plot(v_Blocaux.Time/60,v_Blocaux.Data(:,index_90kV_nodes))
hold on;
plot(v_Blocaux.Time/60,v_Blocaux.Data(:,index_20kV_nodes))
plot(v_Blocaux.Time/60,v_Blocaux.Data(:,index_225kV_nodes))
% plot(v_Blocaux.Time/60,v_Blocaux.Data(:,index_225kV_nodes),'LineWidth',2)
title('Voltages')
grid;
xlabel('Time [min]')
ylabel('Voltage [p.u.]')
xlim([0,1600/60])
ylim([0.94 1.12])
yticks(0.94:0.03:1.12)
xticks([0,5,10,15,20,25])
%% reactive power setpoints
subplot(2,3,4)
plot(q_Blocaux.Time/60,q_Blocaux.Data)
title('Reactive Power')
grid;
xlabel('Time [min]')
ylabel('Reactive Power Setpoints [MVAr]')
xlim([0,1600/60])
xticks([0,5,10,15,20,25])
% ylim([-10 5])

%% matlab2tikz
cleanfigure('targetResolution', 300)
matlab2tikz('results_6_panels.tex','height', '\fheight', 'width', '\fwidth','floatFormat','%.3f')
% matlab2tikz('myfigure.tex', 'standalone', true,'floatFormat','%.4f');
