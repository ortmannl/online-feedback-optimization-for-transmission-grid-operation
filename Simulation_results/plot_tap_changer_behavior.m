figure
subplot_gap = 0.02; % gap between panels
subplot_margin = 0.07; % margin between side of figure and panel with space for the legend
subplot_size = (1 - subplot_margin - subplot_gap)/2;% panel size

%% G = 500
h1=subplot(2,2,1);
load("int_tap_every_1_sensi_normal_no_tap_rate_G_tap_500.mat")
blocaux=extract_blocaux(mpm.mpc);
plot(tap_Blocaux_quantitized.Time/60,tap_Blocaux_quantitized.Data)
% xlabel('Time [min]')
ylabel('Tap Rate')
xlim([0,1600/60])
ylim([0.89,1.11])
grid
xticks([0,10,15,20,25])
set(gca,'xticklabel',{[]})
text(1,0.91,'G=500')
set(h1, 'Units', 'normalized');
set(h1, 'Position', [subplot_margin, subplot_margin + subplot_size + subplot_gap, subplot_size, subplot_size]);
%% G = 1000
h2=subplot(2,2,2);
load("int_tap_every_1_sensi_normal_no_tap_rate_G_tap_1000.mat")
plot(tap_Blocaux_quantitized.Time/60,tap_Blocaux_quantitized.Data)
% xlabel('Time [min]')
% ylabel('Tap Rate')
xlim([0,1600/60])
ylim([0.89,1.11])
grid
xticks([0,5,10,15,20,25])
set(gca,'xticklabel',{[]})
set(gca,'yticklabel',{[]})
text(1,0.91,'G=1000')
set(h2, 'Units', 'normalized');
set(h2, 'Position', [subplot_margin + subplot_size + subplot_gap, subplot_margin + subplot_size + subplot_gap, subplot_size, subplot_size]);
%% G = 3000
h3=subplot(2,2,3);
load("int_tap_every_1_sensi_normal_no_tap_rate_G_tap_3000.mat")
plot(tap_Blocaux_quantitized.Time/60,tap_Blocaux_quantitized.Data)
xlabel('Time [min]')
ylabel('Tap Rate')
xlim([0,1600/60])
ylim([0.89,1.11])
grid
xticks([0,5,10,15,20,25])
text(1,0.91,'G=3000')
set(h3, 'Units', 'normalized');
set(h3, 'Position', [subplot_margin, subplot_margin, subplot_size, subplot_size]);
%% G = 5000
h4=subplot(2,2,4);
load("int_tap_every_1_sensi_normal_no_tap_rate_G_tap_5000.mat")
plot(tap_Blocaux_quantitized.Time/60,tap_Blocaux_quantitized.Data)
xlabel('Time [min]')
% ylabel('Tap Ratio')
xlim([0,1600/60])
ylim([0.89,1.11])
grid
xticks([0,5,10,15,20,25])
set(gca,'yticklabel',{[]})
text(1,0.91,'G=5000')
set(h4, 'Units', 'normalized');
set(h4, 'Position', [subplot_margin + subplot_size + subplot_gap, subplot_margin, subplot_size, subplot_size]);



%% matlab2tikz
cleanfigure('targetResolution', 600)
matlab2tikz('tap_changer_behavior.tex','height', '\fheight', 'width', '\fwidth','floatFormat','%.5f')
