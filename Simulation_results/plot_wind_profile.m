load("p_changing_wind_power.mat")
blocaux=extract_blocaux(mpm.mpc);
figure
stairs(p_changing_wind_power.Time/60, p_changing_wind_power.Data(:,1)/blocaux.gen(1,9)*100)
title('Wind Power')
xlabel('Time [min]')
ylabel('Wind Power [%]')
xlim([0,1600/60])
grid
%% matlab2tikz
cleanfigure('targetResolution', 600)
matlab2tikz('wind_profile.tex','height', '\fheight', 'width', '\fwidth','floatFormat','%.5f')
