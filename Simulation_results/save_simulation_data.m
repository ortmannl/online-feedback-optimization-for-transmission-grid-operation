%% Save the simulation data

% Lukas Ortmann 29-12.2020

file_name = ['Simulation_results/' datestr(now) '_Ts_' num2str(sample_time_controller)];


save(file_name, 'mpm', 'q_Blocaux', 'tap_Blocaux_quantitized', 'tap_Blocaux_pre_quantization', 'p_Blocaux', 'v_Blocaux', 'reactive_limits_blocaux', 'sample_time_controller', 'controller_settings', 'v_Blocaux', 'tap_ratios_all','tap_ratios_blocaux','cost_blocaux','constraint_violations_Blocaux','line_usage_Blocaux','curtailment_blocaux','losses_blocaux','losses_all','wind_power');