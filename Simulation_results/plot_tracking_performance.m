%% showing the tracking performance of OFO

% we ran an OPF solver for every time instant to see what the theoretical
% optimum is. To be able to run an OPF solver we fix the tap changers at
% tap ratio 1. We also ran our OFO controller for the exact same setup with
% the exact same model information. Here we plot the overall performance of
% both to show case the tracking behavior of the OFO controller. After all
% the OFO controller tries to track the optimum of the OPF solver.

figure;

%% extract the Blocaux area
blocaux = extract_blocaux(mpm.mpc);

%% plot the available wind power

plot(wind_power.Time/60, wind_power.Data(:,1)/blocaux.gen(1,9)*100)
hold on;

%% OPF
load("OPF_tap_1_every_10sec")
% Percentage of wind power we can use
sum_injected_p = sum(p_Blocaux.Data,2);
sum_curtailed_p = sum(curtailment_blocaux.Data,2);
sum_possible_p = sum_injected_p + sum_curtailed_p;

percentage_wind_power = sum_injected_p ./ sum_possible_p;

plot(wind_power.Time/60, percentage_wind_power .* wind_power.Data(:,1)/blocaux.gen(1,9)*100);

%% OFO
load("perfect_sensi_tap_1_every_10sec")
% Percentage of wind power we can use
sum_injected_p = sum(p_Blocaux.Data,2);
sum_curtailed_p = sum(curtailment_blocaux.Data,2);
sum_possible_p = sum_injected_p + sum_curtailed_p;

percentage_wind_power = sum_injected_p ./ sum_possible_p;

plot(wind_power.Time/60, percentage_wind_power .* wind_power.Data(:,1)/blocaux.gen(1,9)*100);

%% plot settings
ylim([0, 100])
grid;
xlabel('Time [min]')
ylabel('Wind Power [%]')
xlim([0,1600/60])
xticks([0,5,10,15,20,25])
lgd = legend('Available', 'Theoretical optimum', 'OFO');
lgd.Location = 'northwest';

%% matlab2tikz
cleanfigure('targetResolution', 300)
matlab2tikz('tracking_performance.tex','height', '\fheight', 'width', '\fwidth','floatFormat','%.3f')
% matlab2tikz('myfigure.tex', 'standalone', true,'floatFormat','%.4f');
