function gencost = opf_cost_function_helper(mpc, indexes)
%% define the cost function of the OPF problem
    gencost = zeros( 2*indexes.nGen, 7 ); % make an array of 2*nGen lenght. First nGen entries are for active power. Second nGen entries are for reactive power
    gencost( :, 1 ) = 2; % cost model (2 = polynomial)
    gencost( :, 4 ) = 3; % number of parameters for the polynominal (3 = second order polynomial)
    %% cost on q (quadratic cost => change parameter in column 5)
    gencost(indexes.nGen+indexes.blocaux_gens,5) = 0; %parameter k1 of k1*q^2+k2*q+k3 (k2 and k3 are initialized with zero)
    %% cost on p (linear cost => change parameter in column 6)
    gencost(indexes.blocaux_gens,6) = -1; %parameter k2 of k1*p^2+k2*p+k3 (k1 and k3 are initialized with zero)
    gencost(indexes.non_blocaux_gens_slack,6)=1; % we put a positive linear cost on the active power of the slack bus. The solver will make the active power at the slack bus as low as possible. This will minimize the losses
end

