function mpc = opf_delete_gens_change_type(mpc)

%% helper function for OPF_solver.m
% 1. this functions deletes generators that are out of service (off)
% 2. this function changes PV buses without a generator to PQ buses

% delete out-of-service gens
out_of_service_gen_indexes = find(mpc.gen(:,8)==0);
mpc.gen(out_of_service_gen_indexes,:)=[];

% change PV nodes without an active generator to PQ nodes
PV_node_indexes = find(mpc.bus(:,2)==2)';
for i = PV_node_indexes
    if ~ismember(mpc.bus(i,1) , mpc.gen(:,1))
        mpc.bus(i,2) = 1;
    end
end
end