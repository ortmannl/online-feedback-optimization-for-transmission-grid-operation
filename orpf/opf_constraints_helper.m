function mpc = opf_constraints_helper(mpc,constraints,indexes,reactive_limits_blocaux, p_capabilities)
%% This function sets the constraints of the OPF problem

%% set the constraints in the blocaux area

% active power constraints in the blocaux area



if constraints.P_blocaux == 1
    % PV buses
    temp = find(ismember(indexes.blocaux_gens,indexes.blocaux_gens_PV)); % among the blocaux gens, which ones are the PV gens?
    mpc.gen(indexes.blocaux_gens_PV, 9) = p_capabilities(temp); % max active power is maximum what the current wind allows for
    mpc.gen(indexes.blocaux_gens_PV, 10) = 0; % min active power is 0
    % PQ buses
    temp = find(ismember(indexes.blocaux_gens,indexes.blocaux_gens_PQ)); % among the blocaux gens, which ones are the PQ gens?
    mpc.gen(indexes.blocaux_gens_PQ, 9) = p_capabilities(temp); % max active power is maximum what the current wind allows for
    mpc.gen(indexes.blocaux_gens_PQ, 10) = 0; % min active power is 0
end

% voltage magintude constraints in the blocaux area

if constraints.V_blocaux == 1
    
else
    mpc.bus(indexes.blocaux_buses, 12) = Inf;
    mpc.bus(indexes.blocaux_buses, 13) = -Inf; 
end

% reactive power constraints in the blocaux area

if constraints.Q_blocaux == 1
    mpc.gen(indexes.blocaux_gens, 4) = reactive_limits_blocaux.max; % max reactive power
    mpc.gen(indexes.blocaux_gens, 5) = reactive_limits_blocaux.min; % min reactive power
end

%% set the constraints in the rest of france

% active power constraints in the rest of france

if constraints.P_france == 1
    % PV buses
    mpc.gen(indexes.non_blocaux_gens_PV, 9) = mpc.gen(indexes.non_blocaux_gens_PV, 2); % max active power is current active power
    mpc.gen(indexes.non_blocaux_gens_PV, 10) = mpc.gen(indexes.non_blocaux_gens_PV, 2); % min active power is also current active power
    % PQ buses
    mpc.gen(indexes.non_blocaux_gens_PQ, 9) = mpc.gen(indexes.non_blocaux_gens_PQ, 2); % max active power is current active power
    mpc.gen(indexes.non_blocaux_gens_PQ, 10) = mpc.gen(indexes.non_blocaux_gens_PQ, 2); % min active power is also current active power
end

% voltage magintude constraints in the rest of france

if constraints.V_france == 1
    % fix the voltage limits of PV buses at the current voltage
    for i = indexes.non_blocaux_gens_PV % go through all those gens
        connected_bus = mpc.gen(i,1); % find out which bus they are connected to
        index_bus = find(mpc.bus(:,1) == connected_bus); % find the index of that bus
        if mpc.gen(i,8) == 1 % if the gen is active
            mpc.bus(index_bus,12) = mpc.bus(index_bus,8); % fix the max voltage at the current voltage
            mpc.bus(index_bus,13) = mpc.bus(index_bus,8); % fix the min voltage at the current voltage
        end
    end
    mpc.bus(indexes.non_blocaux_buses_PQ,12) = inf; % upper voltage limit
    mpc.bus(indexes.non_blocaux_buses_PQ,13) = -inf; % lower voltage limit
else
%     mpc.bus(indexes.non_blocaux_buses_PV,12) = 1.2; % upper voltage limit
%     mpc.bus(indexes.non_blocaux_buses_PV,13) = 0; % lower voltage limit
end

% reactive power constraints in the rest of france

if constraints.Q_france == 1
    % PQ buses
    mpc.gen(indexes.non_blocaux_gens_PQ, 4) = mpc.gen(indexes.non_blocaux_gens_PQ, 3); % max reactive power is current reactive power
    mpc.gen(indexes.non_blocaux_gens_PQ, 5) = mpc.gen(indexes.non_blocaux_gens_PQ, 3); % min reactive power is also current reactive power
else
    %mpc.gen(indexes.non_blocaux_gens,4) = inf;  % upper reactive power limit
    %mpc.gen(indexes.non_blocaux_gens,5) = -inf; % lower reactive power limit
end

if constraints.V_slack == 1
    mpc.bus(indexes.non_blocaux_buses_slack,12) = mpc.bus(indexes.non_blocaux_buses_slack,8); % constrain the slack bus voltage to its original value
    mpc.bus(indexes.non_blocaux_buses_slack,13) = mpc.bus(indexes.non_blocaux_buses_slack,8); % constrain the slack bus voltage to its original value
end

%% set the line constraints

if constraints.lines_blocaux == 1
    % the values defined in the mpc file are used
else
    mpc.branch(indexes.blocaux_branches,6:8)=0; % the constraints are lifted (0 means no constraint for Matpower)
end

if constraints.lines_france == 1
    % the values defined in the mpc file are used
else
    mpc.branch(indexes.non_blocaux_branches,6:8)=0; % the constraints are lifted (0 means no constraint for Matpower)
end
end

