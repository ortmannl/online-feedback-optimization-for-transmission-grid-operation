function [p,q] = OPF_solver(p_capabilities,reactive_limits_blocaux, v_lim)
    %% This function solves the Optimal Reactive Power Flow problem
    % - The OPF problem is defined
    % - The OPF problem is solved  
    % - The reactive power setpoints for the Blocaux area are extracted
    
    % Lukas Ortmann 08-10.2021

    %% Get mpm object
    mpm = evalin('base','mpm');
    mpc = mpm.mpc;
    
    
    %% delete generators that are inactive and change buses without gens to PQ
    mpc = opf_delete_gens_change_type(mpc);
    
    %% get all kinds of indexes of blocaux and non-blocaux buses and gens
    indexes = index_helper(mpc);
    
    %% define the cost function of the OPF problem
    mpc.gencost = opf_cost_function_helper(mpc, indexes); 
    
    %% choose the constraints for the OPF problem 
    
    % constraints in the blocaux area
    constraints.P_blocaux = 1;
    constraints.V_blocaux = 1;
    constraints.Q_blocaux = 1;

    % constraints in the rest of france
    % set them to 1 so that the rest of the grid is constraint to be PQ,
    % PV, buses etc
    constraints.P_france = 1;
    constraints.V_france = 1;
    constraints.Q_france = 1;
    constraints.V_slack = 1;
    
    % line constraints
    constraints.lines_blocaux = 1;
    constraints.lines_france = 0; % set to 0 so that the Blocaux OPF does not care about line problems in the rest of France
    % have the helper function implement the constraints
    mpc = opf_constraints_helper(mpc, constraints, indexes, reactive_limits_blocaux, p_capabilities);
    
    
    
    %% define the voltage magnitude limits of all 90kV buses that are in the blocaux area
%     mpc.bus( indexes.blocaux_buses_90kV, 12 ) = v_lim.max; % upper voltage limit
%     mpc.bus( indexes.blocaux_buses_90kV, 13 ) = v_lim.min; % lower voltage limit
    
    %% define the voltage magnitude limits of all 20kV buses that are in the blocaux area
%     mpc.bus( indexes.blocaux_buses_20kV, 12 ) = v_lim.max; % upper voltage limit
%     mpc.bus( indexes.blocaux_buses_20kV, 13 ) = v_lim.min; % lower voltage limit
    
    %% define the solver option for the OPF solver
%     mpopt = mpoption('verbose',2,'out.all',2,'opf.ac.solver','MIPS','mips.costtol',10^-9, 'opf.ignore_angle_lim',1,'opf.start',2);
%     mpopt = mpoption('verbose',2,'out.all',2,'opf.start',2,'opf.ac.solver','MIPS','mips.costtol',10^-9); %changing the tolerance to 10^-12 does not change the result
    mpopt = mpoption('verbose',0,'out.all',0,'opf.start',2,'opf.ac.solver','MIPS','mips.costtol',10^-12); %changing the tolerance to 10^-12 does not change the result
%     mpopt = mpoption('verbose',2,'out.all',2,'opf.start',2,'opf.ac.solver','FMINCON'); %changing the tolerance to 10^-12 does not change the result
%    mpopt = mpoption('verbose',0,'out.all',0,'opf.start',2,'opf.ac.solver','FMINCON'); %changing the tolerance to 10^-12 does not change the result
    
    %% solve the OPRF
    
    results = runopf( mpc , mpopt);
    if results.success ~= 1
        print(results.success)
        error('Optimal power flow solver not successful')
    end
    
    q = results.gen(indexes.blocaux_gens,3);
    p = results.gen(indexes.blocaux_gens,2);


end