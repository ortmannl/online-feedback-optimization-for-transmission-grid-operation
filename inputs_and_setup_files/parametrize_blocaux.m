function mpc = parametrize_blocaux(mpc, p_changing_wind_power)

%% Parametrize the Blocaux area
% Set the reactive power limits of the gens in the blocaux area
% Change PV to PQ nodes

indexes = index_helper(mpc);

%% Define active power limits
% RTE changed the active power injections of the original casefile to model
% the future active power injections. They did not change the active power
% limits. We do this now.
mpc.gen(indexes.blocaux_gens,9) = 1.5*mpc.gen(indexes.blocaux_gens,2);


%% Define reactive power limits
% The reactive power limit is 30% of the max active power limit
mpc.gen(indexes.blocaux_gens,4) = 0.3 * mpc.gen(indexes.blocaux_gens,9);
mpc.gen(indexes.blocaux_gens,5) = -0.3 * mpc.gen(indexes.blocaux_gens,9);

%% Change PV nodes to PQ nodes
% Change PV nodes in the blocaux area to PQ nodes so we can control their
% reactive power ouput and they can be part of our control scheme 
mpc.bus(indexes.blocaux_buses_PV,2) = 1;

%% Set the initial tap changer positions in the Blocaux area to 1
idx_branches_all_tap_changers = find_OLTC_indexes(mpc);
index_blocaux_OLTCs_in_all_OTLCs = [4, 139, 140, 141, 166, 167, 774, 775, 5, 6]; % all
index_blocaux_OLTCs_in_branch = idx_branches_all_tap_changers(index_blocaux_OLTCs_in_all_OTLCs);

mpc.branch(index_blocaux_OLTCs_in_branch,9) = 1;

%% Set the initial active power of the wind farms
mpc.gen(indexes.blocaux_gens,2) = p_changing_wind_power.Data(1,:);

%% Set the power consumption to 0
mpc.bus(indexes.blocaux_buses,3) = 0;
end