%% Initialisation of the controllers

% Lukas Ortmann 22-12.2020

%% Define Controller Settings

% choose the controller
% which_controller = 'ORPF'; % Optimal Reactive Power Flow
which_controller = 'FOC'; % Feedback Optimization Controller

% sampling time
sample_time_controller = 1;

% sensitivity for Online Feedback Optimization Controller
% all these files contain sensitivities used by the OFO controller.
% the sensitivity differ in the opreating point they were calculated in and
% how they were calculated
%load('sensitivity_numerically')
% load('sensitivity_closed_form');
% load('sensitivity_closed_form_with_OLTCs');
% load('sensitivity_all_inputs');
% load('sensitivity_all_inputs_tripped_transformers');
load('sensitivity_at_start')
% load('sensitivity_all_inputs_at_line_limit_523_939')

% reactive power limits
blocaux = extract_blocaux(mpm.mpc);
reactive_limits_blocaux.max = blocaux.gen(:,4);
reactive_limits_blocaux.min = blocaux.gen(:,5);

% tighter voltage limits

v_lim.max = 1.05;
v_lim.min = 0.95;

%% cost function
price_MWh = 1;
% controller_settings.weight_max_p = 1* price_MWh; % cost of curtailing 1MW
omega_base_20kV = 20000^2/(mpm.mpc.baseMVA*10^6);
resistance_15km_line_copper_240mm2 = 0.02*15000/240;
resistance_15km_line_per_unit = resistance_15km_line_copper_240mm2/omega_base_20kV;
controller_settings.weight_q = resistance_15km_line_per_unit/1.1^2/mpm.mpc.baseMVA * price_MWh; % take the resistance of a 15km copper cable with cross section 240mm2. The current flows through all three phases and the losses on that line due to reactive power are 3*R*I^2 where I = Q/(sqrt(3)*U) and the voltage is at the upper limit whenever we use reactive power aka U is roughly 1.1. Plugging all  that in gives the weight
% controller_settings.weight_tap = 0;


%% Number OLTCs
number_gens = length(blocaux.gen(:,1)); % find the number of gens in the blocaux area
number_OLTCs = 10;

%% Define metric for the projection

G_q = .1*ones(number_gens,1); % 0.01 with perfect_sensitivity & 0.1 with constant approximate sensitivity & 0.02 with perfect sensitivty and blocked tap changers
G_p = .2*ones(number_gens,1);
G_tap = 2500*ones(number_OLTCs,1);
G = diag([G_q;G_p;G_tap]);

%% build struct
controller_settings.v_lim = v_lim;
controller_settings.sensitivity_vp = sensitivity_vp;
controller_settings.sensitivity_vq = sensitivity_vq;
controller_settings.sensitivity_vtap = sensitivity_vtap;
controller_settings.sensitivity_fp = sensitivity_fp;
controller_settings.sensitivity_fq = 0*sensitivity_fq;
controller_settings.sensitivity_ftap = sensitivity_ftap;
controller_settings.sensitivity_ap = sensitivity_ap;
controller_settings.sensitivity_aq = sensitivity_aq;
controller_settings.sensitivity_atap = sensitivity_atap;
controller_settings.sensitivity_losses.vm = sensitivity_losses.vm;
controller_settings.sensitivity_losses.va = sensitivity_losses.va;
controller_settings.G = G;
%%
% decide whether to use perfect senstivities
% 1 = perfect & 0 = constant sensitivities
controller_settings.perfect_sensitivity = 0;