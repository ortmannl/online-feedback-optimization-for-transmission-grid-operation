function mpc = parametrize_case_file(mpc)

%% Parametrize the case file
% 1. Set the reactive and active power limits of the slack bus
% 2. Activate/Deactivate certain lines/transformers

indexes = index_helper(mpc);

%% Define reactive and active power limit of the slack bus
mpc.gen(indexes.non_blocaux_gens_slack,4) = inf; % max reactive power
mpc.gen(indexes.non_blocaux_gens_slack,5) = -inf; % min reactive pwer
mpc.gen(indexes.non_blocaux_gens_slack,9) = inf; % max active power
mpc.gen(indexes.non_blocaux_gens_slack,10) = -inf; % min active power

%% Set the reactive power of the buses in the Blocaux area to 0
mpc.bus(indexes.blocaux_buses_PQ, 4) = 0;


%% Set active power generation at bus 1443 to 0
% mpc.gen(307,2)=0;

%% Deactivate lines/transformers
% line = [523 5734];
% line = [617 615];
% line = [1442 1443];
% line = [524 1654];
% index_line = find( (mpc.branch(:,1)==line(1) & mpc.branch(:,2)==line(2)) | (mpc.branch(:,1)==line(2) & mpc.branch(:,2)==line(1)));
% index_line = [8260; 8261; 8262];
% index_line = 8262;
% mpc.branch(index_line,11)=0;

end