function [p, q, tap , constraint_violations,grad_norm]= FO_control(measured_voltage, measured_flows, measured_q, measured_tap, measured_p, p_capabilities, blocaux, controller_settings)
%% Online Feedback Optimization controller

% This code runs an Online Feedback Optimization controller for the blocaux
% area

% Lukas Ortmann 26.05.2022

% 1. Define auxiliary variables
% 2. Define voltage limits
% 3. Define flow limits on the lines
% 4. Define active and reactive power limits
%% define auxiliary variables
number_gens = length(blocaux.gen(:,1)); % find the number of gens in the blocaux area
number_buses = length(blocaux.bus(:,1)); % find the number of buses in the blocaux area
number_OLTCs = 10;
number_branches = length(blocaux.branch(:,1)); % find the number of branches in the blocaux area

%% voltage limits
v_lim = controller_settings.v_lim;

blocaux_20kV_buses = find(blocaux.bus(:,10)==20); % find 20kV buses
blocaux_90kV_buses = find(blocaux.bus(:,10)==90); % find 90kV buses
blocaux_225kV_buses = find(blocaux.bus(:,10)==225); % find 225kV buses

% get the normal voltage limits
upper_limit_voltage = blocaux.bus(:,12);
% change voltage limits of 20kV buses
% upper_limit_voltage(blocaux_20kV_buses) = v_lim.max;
% change voltage limits of 90kV buses
% upper_limit_voltage(blocaux_90kV_buses) = v_lim.max;
% change voltage limits of 225kV buses
upper_limit_voltage(blocaux_225kV_buses) = v_lim.max;

% get the normal voltage limits
lower_limit_voltage = blocaux.bus(:,13);
% change voltage limits of 20kV buses
% lower_limit_voltage(blocaux_20kV_buses) = v_lim.min;
% change voltage limits of 90kV buses
% lower_limit_voltage(blocaux_90kV_buses) = v_lim.min;
% change voltage limits of 225kV buses
lower_limit_voltage(blocaux_225kV_buses) = v_lim.min;

%% flow limits
% RTE only uses the lines up until 90% capacity because there is a 45sec
% delay before active power curtailment becomes active and the 10% are a
% safety margin. RTE does not expect a change in renewable generation and
% load within 45 sec that would overload the line with this 10% safety
% margin
% upper_limit_flows = 0.9*blocaux.branch(:,6);
% lower_limit_flows = -0.9*blocaux.branch(:,6);
upper_limit_flows = blocaux.branch(:,6);
lower_limit_flows = -blocaux.branch(:,6);
% same flow limits are 0 which means no limit. We need to change those
% limit to inf, because the QP is of course infeasible if we leave the
% limit at 0
for i = 1:length(upper_limit_flows)
   if blocaux.branch(i,6) == 0
      upper_limit_flows(i) = inf;
      lower_limit_flows(i) = -inf;
   end
end


%% define constraints

% reactive power limits

q_max = blocaux.gen(:,4); % get max reactive power capabilities
% q_max = zeros(number_gens,1);
q_min = -blocaux.gen(:,4); % get min reactive power capabilities
% q_min = zeros(number_gens,1);

% curtailment limit
p_max = p_capabilities;
% p_max = measured_p
p_min = zeros(number_gens,1);
% p_min = measured_p;


%% get sensitivity from controller_settings structure and build nabla_h
if controller_settings.perfect_sensitivity == 1
    controller_settings = update_sensitivity(controller_settings);
end
sensitivity_vp = controller_settings.sensitivity_vp;
sensitivity_vq = controller_settings.sensitivity_vq;
sensitivity_vtap = controller_settings.sensitivity_vtap;
sensitivity_fp = controller_settings.sensitivity_fp;
sensitivity_fq = controller_settings.sensitivity_fq;
sensitivity_ftap = controller_settings.sensitivity_ftap;

nabla_h_voltage = [sensitivity_vq, sensitivity_vp, sensitivity_vtap]; % sensitivity of the bus voltages with respect to all inputs
nabla_h_flows = [sensitivity_fq, sensitivity_fp, sensitivity_ftap]; % sensitivity of the line flows with respect to all inputs
nabla_h = [nabla_h_voltage; nabla_h_flows];



%% get the const function derivative
nabla_Phi = cost_function_derivative(measured_q,controller_settings);


%% tap changer limits
tap_max = 1.1*ones(number_OLTCs,1);
% tap_max = measured_tap;
tap_min = 0.9*ones(number_OLTCs,1);
% tap_min = measured_tap;

% deactivate the tap changer in the Blocaux substation
% tap_max(2:4) = measured_tap(2:4);
% tap_min(2:4) = measured_tap(2:4);


%% build input and output constraint matrices and vectors

% input

A = [eye(2*number_gens+number_OLTCs);...
    -eye(2*number_gens+number_OLTCs)];

b = [q_max; p_max; tap_max; -q_min; -p_min; -tap_min];

% input constraints + rate limit for OLTCs
A_rate = A;
max_number_tap_changes_per_iteration = inf;
tap_rate_max = max_number_tap_changes_per_iteration * 0.1/16 * ones(number_OLTCs,1);
tap_rate_min = -tap_rate_max;

delta_p_rate_max = inf * ones(number_gens,1);
delta_p_rate_min = -inf * ones(number_gens,1);
q_rate_max = inf * ones(number_gens,1);
q_rate_min = -inf * ones(number_gens,1);

b_rate = [q_rate_max; delta_p_rate_max; tap_rate_max; -q_rate_min; -delta_p_rate_min; -tap_rate_min];

% output

C = [eye(number_buses+number_branches);...
    -eye(number_buses+number_branches)
    ];

d = [upper_limit_voltage; upper_limit_flows; -lower_limit_voltage; -lower_limit_flows];
%% set up the cost function of the projection
f = nabla_Phi;
G = controller_settings.G;


%% Constraints Aq*w <= bq for the QP or MIQP solver

Aq=[  A;...
           C*nabla_h;...
            A_rate];
bq=[b-A*[measured_q; measured_p; measured_tap];...
    d-C*[measured_voltage; measured_flows];...
    b_rate];

%% Compute step by solving the QP or MIQP

% w = OFO_without_integer(Aq,bq,G,f); % standard Verena's algorithm
w = OFO_with_integer(Aq,bq,G,f); % New OFO algorithm with integer constraints on the tap changer update
%% update the setpoints
q = measured_q + w(1:number_gens);
p = measured_p + w(number_gens+1:2*number_gens);
tap = measured_tap + w(2*number_gens+1:end);

%% constraint violation
constraint_violations = double(Aq*zeros(94,1)>bq); % check which constraints were violated before the new step is going to be applied
grad_norm = f(1:number_gens);
end
