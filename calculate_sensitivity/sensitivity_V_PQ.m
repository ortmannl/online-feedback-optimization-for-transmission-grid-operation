function [sensitivity_vp,sensitivity_vq] = sensitivity_V_PQ(mpc)
% This function takes a Matpower case and returns the sensitivity of the
% voltages in the Blocaux area with respect to the active injections in the
% Blocaux area

% The equations come from
% [1] https://doi.org/10.1109/ALLERTON.2015.7447032

% complex number
i = complex(0,1);
mpopt = mpoption('verbose',0,'out.all',0);
mpc = runpf(mpc,mpopt);
if mpc.success ~= 1
    error('Solver did not converge')
end

Y = makeYbus(ext2int(mpc)); % get the Y matrix from Matpower
n = length(Y); % number buses in the grid

% see underneath equation 5 in [1]
N = sparse(1:2*n,1:2*n,[ones(1,n), -ones(1,n)],2*n,2*n);
  
v = mpc.bus(:,8); % get all the bus voltages
theta = mpc.bus(:,9)/180*pi;

% see underneath equation 5 in [1]
u = v .* exp(i*theta);

% equation 5 in [1]

A_x =[ ( bracket( sparse(1:n,1:n,conj(Y*u),n,n) ) + bracket(sparse(1:n,1:n,u,n,n)) * N * bracket( Y ) ) * R(u), sparse(1:2*n,1:2*n,-ones(1,2*n),2*n,2*n) ];
%% build p and q vector

p = zeros(n,1);
q = zeros(n,1);

for i=1:n
    p(i) = -mpc.bus(i,3); % consumption at buses is a positive value => negative to calculate overall generation at that bus
    q(i) = -mpc.bus(i,4); % consumption at buses is a positive value => negative to calculate overall generation at that bus
    index_connected_gens = find(mpc.gen(:,1) == mpc.bus(i,1));
    if ~isempty(index_connected_gens) % if there are gens connected to the bus we need to add their generation to the overall generation at that bus
        p(i) = p(i) + sum(mpc.gen(index_connected_gens,2) .* mpc.gen(index_connected_gens,8)); % sum the generation, but multiply with the generator status first (status= 0 if gen is offline) (probably not needed)
        q(i) = q(i) + sum(mpc.gen(index_connected_gens,3) .* mpc.gen(index_connected_gens,8)); % sum the generation, but multiply with the generator status first (status= 0 if gen is offline) (probably not needed)
    end
end

%% build E matrix and d vector
E = sparse(2*n,4*n);
d=zeros(2*n,1);
for i = 1:n
    if mpc.bus(i,2) == 1 % bus i is a PQ bus
        E(2*i-1, 2*n+i) = 1;
        d(2*i-1,1) = p(i);
        E(2*i, 3*n+i) = 1;
        d(2*i,1) = q(i);
    elseif mpc.bus(i,2) == 2 % bus i is a PV bus
        E(2*i-1, 2*n+i) = 1;
        d(2*i-1,1) = p(i);
        E(2*i, 0*n+i) = 1;
        d(2*i,1) = v(i);
    elseif mpc.bus(i,2) == 3 % bus i is a slack bus
        E(2*i-1, 0*n+i) = 1;
        d(2*i-1,1) = v(i);
        E(2*i, 1*n+i) = 1;
        d(2*i,1) = theta(i);
    else
        error('Unknown bus type')
    end
end


Phi = [A_x; E];
%% split Phi into the blocaux buses and the non-blocaux buses
blocaux = extract_blocaux(mpc);
blocaux_buses = ismember(mpc.bus(:,1),blocaux.bus(:,1)); % find all the buses in the blocaux area
index_blocaux_buses = find(blocaux_buses == 1)'; % find the indexes of the buses that are not in the Blocaux area

% COLUMNS
% x = [v, theta, p , q] therefore the first n entries are voltages, the
% sencond n entries are angles, the third n entries are active powers and
% the reactive power injections are in the entries following the first 3*n
% entries. Therefore the active power columns we want to delete or extract are 2*n + the indexes of
% the blocaux buses and the reactive power columns we want to delete or extract are 3*n + the indexes of
% the blocaux buses 

columns = [2*n+index_blocaux_buses, 3*n+index_blocaux_buses];

% ROWS
% The first 2n rows of Phi_tilde are power flow equations
% The second 2n rows are the constraints from PV, PQ and slack buses
% We want to delete rows in this second part
% The constraints are build as follows. It is first the active power
% constraint and then the reactive power constraint.
% The rows we want to delete are therefore 2*n + 2*index_blocaux_buses-1
% if we want to delete active power rows and 2*n + 2*
% index_blocaux_buses if we want to delete reactive rows
rows = [2*n+2*index_blocaux_buses-1, 2*n+2*index_blocaux_buses];

% extract Phi_q
Phi_q = Phi(:,columns);
Phi_q(rows,:) = [];

% delete the columns and rows corresponding to the blocaux reactive power
% injections
Phi_tilde = Phi;
Phi_tilde(:,columns) = []; % delete blocaux reactive power columns
Phi_tilde(rows,:) = []; % delte blocaux reactive power rows

%% calculate inv(Phi_tilde)*Phi_q, but in a smart way
X = -Phi_tilde \ Phi_q; % way faster then calculating the inverse


%% extract the rows that are of interest for us (the blocaux rows)
nb = length(blocaux.gen);
bus_index = zeros(1,nb);
% go through all generators and look up the number of the blocaux bus they
% are connected to
for i = 1:nb
   bus_index(i) = find(blocaux.bus(:,1) == blocaux.gen(i,1)); 
end

% extract the blocaux buses from the large sensitivity matrix
X_vp = X(index_blocaux_buses,1:end/2);
% repeat columns of buses with multiple generators and discard columns of buses without generators 
X_vp = X_vp(:,bus_index);

% extract the blocaux buses from the large sensitivity matrix
X_vq = X(index_blocaux_buses,end/2+1:end);
% repeat columns of buses with multiple generators and discard columns of buses without generators 
X_vq = X_vq(:,bus_index);

%% Finally scale the sensitivity with the baseMVA
% Matpower uses the p.u. system. Therefore a change of q=1 means that you
% change q by the baseMVA (in the french case it is 100 MVA). If we want to
% know the change of v with respect to a change of 1 MVAr then we need to
% change q by 0.01 in p.u.
% Therefore, to get the sensitivity that is not in p.u., but in absolute
% values we have to devide the sensitivy by baseMVA

X_vq = X_vq / mpc.baseMVA;
X_vp = X_vp / mpc.baseMVA;

sensitivity_vq = full(X_vq);
sensitivity_vp = full(X_vp);

%% helper functions
% see underneath equation 5 of [1]
function output = bracket(A)
    output = [ real(A) -imag(A);...
               imag(A) real(A)];
end

% see underneath equation 5 of [1]
function output = R(u)
    v_R = abs(u);
    theta_R = angle(u);
    n_R = length(v_R);
    output = [ sparse(1:n_R,1:n_R,cos(theta_R),n_R,n_R) sparse(1:n_R,1:n_R,-v_R.*sin(theta_R),n_R,n_R);...
               sparse(1:n_R,1:n_R,sin(theta_R),n_R,n_R) sparse(1:n_R,1:n_R,v_R.*cos(theta_R),n_R,n_R)];
end


end

