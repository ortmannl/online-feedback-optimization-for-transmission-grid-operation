function [controller_settings] = update_sensitivity(controller_settings)
mpm = evalin('base','mpm');
indexes = evalin('base','indexes');

[controller_settings.sensitivity_vq, controller_settings.sensitivity_aq, controller_settings.sensitivity_fq,controller_settings.sensitivity_losses_q] = update_sensitivity_q(mpm,indexes);
[controller_settings.sensitivity_vp, controller_settings.sensitivity_ap, controller_settings.sensitivity_fp,controller_settings.sensitivity_losses_p] = update_sensitivity_p(mpm,indexes);
[controller_settings.sensitivity_vtap, controller_settings.sensitivity_atap, controller_settings.sensitivity_ftap,controller_settings.sensitivity_losses_tap] = update_sensitivity_tap(mpm,indexes);
% controller_settings.sensitivity_losses = update_sensitivity_losses(mpm);

end

