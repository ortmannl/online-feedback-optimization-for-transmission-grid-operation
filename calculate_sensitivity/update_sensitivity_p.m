function [sensitivity_vp, sensitivity_ap, sensitivity_fp, sensitivity_losses_p] = update_sensitivity_p(mpm,indexes)
mpc=mpm.mpc;
blocaux = extract_blocaux(mpc); % extract the Blocaux area
j = complex(0,1);

%% sensitivity with respect to active power
sensitivity_ap = zeros(length(blocaux.bus),length(blocaux.gen));
sensitivity_vp = zeros(length(blocaux.bus),length(blocaux.gen));
sensitivity_fp = zeros(length(blocaux.branch),length(blocaux.gen));
sensitivity_losses_p = zeros(length(blocaux.gen),1);
% find the blocaux buses in the french grid and save the indexes
% this is later needed to extract the bus voltages of the blocaux buses
% from the french grid
for i = 1:size(blocaux.bus(:,1))
    blocaux_bus_indexs(i) = find(mpc.bus(:,1) == blocaux.bus(i,1));
end

% find the blocaux branches in the french grid and save the indexes
% this is later needed to extract the flows of the blocaux branches
% from the french grid

blocaux_branch_indexes = [];
i = 1;
while i <= length(blocaux.branch(:,1))
    temp_index = find(mpc.branch(:,1) == blocaux.branch(i,1) & mpc.branch(:,2) == blocaux.branch(i,2));
    blocaux_branch_indexes =  [blocaux_branch_indexes; temp_index];
    i = i + length(temp_index);
end

[number_gens,~]=size(blocaux.gen);
for i=1:number_gens % go through all the generators and change their active power one after the other
   mpc_step = mpc; % make a copy of the french grid within which we change the active power
   original_p = mpc.gen(indexes.blocaux_gens(i),2); % get the active power at that bus
   new_p = original_p + .001; % change the active power at that bus by +1 MVAR
   mpc_step.gen(indexes.blocaux_gens(i),2) = new_p; % change the active power at that gen to the new value
   [mpc_step, FLAG] = runpf(mpc_step,mpm.mpopt); % solve the power flow with the changed active power
   if FLAG ~= 1 % throw an error if the power flow is not solved
        error('Power Flow not solved');
   end
   %% sensitivity angles
   original_voltage_angles = mpc.bus(blocaux_bus_indexs,9); % get the original voltage angle at that bus
   new_voltage_angles = mpc_step.bus(blocaux_bus_indexs,9); % get the new voltage angle at the bus
   sensitivity_ap(:,i) = (new_voltage_angles - original_voltage_angles)/(new_p-original_p);
   %% sensitivity voltage magnitudes
   original_voltages = mpc.bus(blocaux_bus_indexs,8); % get the original voltage at that bus
   new_voltages = mpc_step.bus(blocaux_bus_indexs,8); % get the new voltage at the bus
   sensitivity_vp(:,i) = (new_voltages - original_voltages)/(new_p-original_p);
   %% sensitivity flows
   original_flows = abs(mpc.branch(blocaux_branch_indexes,14) + j*mpc.branch(blocaux_branch_indexes,15)); % get the original flow through that branch
   new_flows = abs(mpc_step.branch(blocaux_branch_indexes,14) + j*mpc_step.branch(blocaux_branch_indexes,15)); % get the new flow through that branch
   sensitivity_fp(:,i) = (new_flows - original_flows)/(new_p-original_p);
   %% senstivity_losses
   original_all_losses = get_losses(mpc); % Matpower function that returns the losses in the whole grid
   original_losses = real(sum(original_all_losses(indexes.blocaux_branches))); % take only the losses on the branches inside the Blocaux area, sum them up and only take the active power losses and not the reactive "losses"
   new_all_losses = get_losses(mpc_step); % Matpower function that returns the losses in the whole grid
   new_losses = real(sum(new_all_losses(indexes.blocaux_branches))); % take only the losses on the branches inside the Blocaux area, sum them up and only take the active power losses and not the reactive "losses"
   sensitivity_losses_p(i,1) = (new_losses - original_losses)/(new_p-original_p);
end
end
