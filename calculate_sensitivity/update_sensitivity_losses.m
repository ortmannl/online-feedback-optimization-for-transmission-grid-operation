function [sensitivity_losses] = update_sensitivity_losses(mpm)
mpc=mpm.mpc;
baseMVA = mpc.baseMVA;

blocaux = extract_blocaux(mpc); % extract the Blocaux area


%% find the blocaux branches in the french grid and save the indexes

blocaux_branch_indexes = [];
i = 1;
while i <= length(blocaux.branch(:,1))
    temp_index = find(mpc.branch(:,1) == blocaux.branch(i,1) & mpc.branch(:,2) == blocaux.branch(i,2));
    blocaux_branch_indexes =  [blocaux_branch_indexes; temp_index];
    i = i + length(temp_index);
end

%% find the blocaux buses in the french grid and save the indexes
for i = 1:length(blocaux.bus(:,1))
    blocaux_bus_indexes(i) = find(mpc.bus(:,1) == blocaux.bus(i,1));
end
%% sensitivity of voltage with respect to active power

% get the sensitivity of all branch losses w.r.t. all voltages from
% Matpower
[~,~,~,sensitivity_losses_all] = get_losses(mpc);

% get the real power losses in the Blocaux area w.r.t. to the voltage magnitudes in
% the Blocaux area
sensitivity_losses.vm = full(real(sum(sensitivity_losses_all.m(blocaux_branch_indexes,blocaux_bus_indexes))))/baseMVA;

% get the real power losses in the Blocaux area w.r.t. to the voltage angles in
% the Blocaux area
sensitivity_losses.va = full(real(sum(sensitivity_losses_all.a(blocaux_branch_indexes,blocaux_bus_indexes))))/baseMVA;
end
