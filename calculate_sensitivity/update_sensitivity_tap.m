function [sensitivity_vtap,sensitivity_atap,sensitivity_ftap,sensitivity_losses_tap] = update_sensitivity_tap(mpm,indexes)
mpc=mpm.mpc;
blocaux = extract_blocaux(mpc); % extract the Blocaux area
j = complex(0,1);

idx_branches_all_tap_changers = find_OLTC_indexes(mpc);
index_blocaux_OLTCs_in_all_OTLCs = [4, 139, 140, 141, 166, 167, 774, 775, 5, 6]; % all
index_blocaux_OLTCs_in_branch = idx_branches_all_tap_changers(index_blocaux_OLTCs_in_all_OTLCs);
number_oltcs_in_blocaux = length(index_blocaux_OLTCs_in_all_OTLCs);

%% sensitivity with respect to tap position
sensitivity_atap = zeros(length(blocaux.bus),number_oltcs_in_blocaux);
sensitivity_vtap = zeros(length(blocaux.bus),number_oltcs_in_blocaux);
sensitivity_ftap = zeros(length(blocaux.branch),number_oltcs_in_blocaux);
sensitivity_losses_tap = zeros(number_oltcs_in_blocaux,1);
% find the blocaux buses in the french grid and save the indexes
% this is later needed to extract the bus voltages of the blocaux buses
% from the french grid
for i = 1:size(blocaux.bus(:,1))
    blocaux_bus_indexs(i) = find(mpc.bus(:,1) == blocaux.bus(i,1));
end

% find the blocaux branches in the french grid and save the indexes
% this is later needed to extract the flows of the blocaux branches
% from the french grid


blocaux_branch_indexes = [];
i = 1;
while i <= length(blocaux.branch(:,1))
    temp_index = find(mpc.branch(:,1) == blocaux.branch(i,1) & mpc.branch(:,2) == blocaux.branch(i,2));
    blocaux_branch_indexes =  [blocaux_branch_indexes; temp_index];
    i = i + length(temp_index);
end

[number_tap_changers,~]=size(index_blocaux_OLTCs_in_branch);
parfor i=1:number_tap_changers % go through all the tap changers and change the tap one by one
   mpc_step = mpc; % make a copy of the french grid within which we change the tap
   original_tap = mpc.branch(index_blocaux_OLTCs_in_branch(i),9); % get the original tap at that bus
   new_tap = original_tap + .001; % change the tap of that OLTC by +.001 
   mpc_step.branch(index_blocaux_OLTCs_in_branch(i),9) = new_tap; % change the tap position at that OLTC to the new value
   [mpc_step, FLAG] = runpf(mpc_step,mpm.mpopt); % solve the power flow with the changed tap position
   if FLAG ~= 1 % throw an error if the power flow is not solved
        error('Power Flow not solved');
   end
   %% senstivity voltage angles
   original_voltage_angles = mpc.bus(blocaux_bus_indexs,9); % get the original voltage angle at that bus
   new_voltage_angles = mpc_step.bus(blocaux_bus_indexs,9); % get the new voltage angle at the bus
   sensitivity_atap(:,i) = (new_voltage_angles - original_voltage_angles)/(new_tap-original_tap);
   %% sensitivity voltage magnitudes
   original_voltages = mpc.bus(blocaux_bus_indexs,8); % get the original voltage at that bus
   new_voltages = mpc_step.bus(blocaux_bus_indexs,8); % get the new voltage at the bus
   sensitivity_vtap(:,i) = (new_voltages - original_voltages)/(new_tap-original_tap);
   %% sensitivity flows
   original_flows = abs(mpc.branch(blocaux_branch_indexes,14) + j*mpc.branch(blocaux_branch_indexes,15)); % get the original flow through that branch
   new_flows = abs(mpc_step.branch(blocaux_branch_indexes,14) + j*mpc_step.branch(blocaux_branch_indexes,15)); % get the new flow through that branch
   sensitivity_ftap(:,i) = (new_flows - original_flows)/(new_tap-original_tap);
   %% senstivity_losses
   original_all_losses = get_losses(mpc); % Matpower function that returns the losses in the whole grid
   original_losses = real(sum(original_all_losses(indexes.blocaux_branches))); % take only the losses on the branches inside the Blocaux area, sum them up and only take the active power losses and not the reactive "losses"
   new_all_losses = get_losses(mpc_step); % Matpower function that returns the losses in the whole grid
   new_losses = real(sum(new_all_losses(indexes.blocaux_branches))); % take only the losses on the branches inside the Blocaux area, sum them up and only take the active power losses and not the reactive "losses"
   sensitivity_losses_tap(i,1) = (new_losses - original_losses)/(new_tap-original_tap);
end

end
