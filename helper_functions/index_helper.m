function indexes = index_helper(mpc)

    %% helper function
    % This functions derives all kinds of indexes of the generators and
    % buses in the Blocaux area and the rest of France.
    
    % extract the blocaux area
    blocaux = extract_blocaux(mpc);

    % number of gens in the whole grid
    indexes.nGen = length(mpc.gen(:,1));
    indexes.nBranches = length(mpc.branch(:,1));
    %% find the indexes of the generators
    blocaux_gens = ismember(mpc.gen(:,1),blocaux.gen(:,1));
    % invert the logicals to get the gens that are not in the blocaux area
    non_blocaux_gens = ~blocaux_gens;

    indexes.non_blocaux_gens = find(non_blocaux_gens == 1)'; % find the indexes of the gens that are in the rest of France
    indexes.blocaux_gens = find(blocaux_gens == 1)'; % find the indexes of the gens that are in the Blocaux area
    
    
    %% find the indexes of the buses
    blocaux_buses = ismember(mpc.bus(:,1),blocaux.bus(:,1));
    % invert the logicals to get the buses that are not in the blocaux area
    non_blocaux_buses = ~blocaux_buses;
    indexes.blocaux_buses = find(blocaux_buses == 1)'; % find the indexes of the buses that are in the Blocaux area
    indexes.non_blocaux_buses = find(non_blocaux_buses == 1)'; % find the indexes of the buses that are in the rest of France
    
    %% find the indexes of the branches in the Blocaux area
    blocaux_branches = [];
    i = 1;
    while i <= length(blocaux.branch(:,1))
        temp_index = find(mpc.branch(:,1) == blocaux.branch(i,1) & mpc.branch(:,2) == blocaux.branch(i,2));
        blocaux_branches =  [blocaux_branches; temp_index];
        i = i + length(temp_index);
    end
    indexes.blocaux_branches = blocaux_branches;
    
    %% find the indexes of the branches in the rest of France
    indexes.non_blocaux_branches = setxor(1:indexes.nBranches,indexes.blocaux_branches); % uses setxor from Matlab. setxor(A,B) returns the data of A and B that are not in their intersection (the symmetric difference), with no repetitions. That is, setxor returns the data that occurs in A or B, but not both. C is in sorted order.
    
    %% find the different voltage levels
    indexes.blocaux_buses_20kV = indexes.blocaux_buses ( find(mpc.bus(indexes.blocaux_buses , 10) == 20) );
    indexes.blocaux_buses_90kV = indexes.blocaux_buses ( find(mpc.bus(indexes.blocaux_buses , 10) == 90) );
    indexes.blocaux_buses_225kV = indexes.blocaux_buses ( find(mpc.bus(indexes.blocaux_buses , 10) == 225) );
    indexes.blocaux_buses_380kV = indexes.blocaux_buses ( find(mpc.bus(indexes.blocaux_buses , 10) == 380) );
    
    %% PV buses in the blocaux area
    indexes.blocaux_buses_PV = indexes.blocaux_buses ( find(mpc.bus(indexes.blocaux_buses , 2) == 2) );
    %% PQ buses in the blocaux area
    indexes.blocaux_buses_PQ = indexes.blocaux_buses ( find(mpc.bus(indexes.blocaux_buses , 2) == 1) );
    
    %% PV buses in the rest of France
    indexes.non_blocaux_buses_PV = indexes.non_blocaux_buses ( find(mpc.bus(indexes.non_blocaux_buses , 2) == 2) );
    %% PQ buses in the rest of France
    indexes.non_blocaux_buses_PQ = indexes.non_blocaux_buses ( find(mpc.bus(indexes.non_blocaux_buses , 2) == 1) );
    %% slack buses in the rest of France
    indexes.non_blocaux_buses_slack = indexes.non_blocaux_buses ( find(mpc.bus(indexes.non_blocaux_buses , 2) == 3) );
    
    %% PQ gens in the blocaux area
    temp_logical = ismember(mpc.gen(indexes.blocaux_gens,1), mpc.bus(indexes.blocaux_buses_PQ,1));
    indexes.blocaux_gens_PQ = indexes.blocaux_gens(temp_logical);
    
    %% PV gens in the blocaux area
    temp_logical = ismember(mpc.gen(indexes.blocaux_gens,1), mpc.bus(indexes.blocaux_buses_PV,1));
    indexes.blocaux_gens_PV = indexes.blocaux_gens(temp_logical);
    
    %% PQ gens in the rest of France
    temp_logical = ismember(mpc.gen(indexes.non_blocaux_gens,1), mpc.bus(indexes.non_blocaux_buses_PQ,1));
    indexes.non_blocaux_gens_PQ = indexes.non_blocaux_gens(temp_logical);
    
    %% PV gens in the rest of France
    temp_logical = ismember(mpc.gen(indexes.non_blocaux_gens,1), mpc.bus(indexes.non_blocaux_buses_PV,1));
    indexes.non_blocaux_gens_PV = indexes.non_blocaux_gens(temp_logical);
    
    %% Slack gens in the rest of France
    temp_logical = ismember(mpc.gen(indexes.non_blocaux_gens,1), mpc.bus(indexes.non_blocaux_buses_slack,1));
    indexes.non_blocaux_gens_slack = indexes.non_blocaux_gens(temp_logical);
end