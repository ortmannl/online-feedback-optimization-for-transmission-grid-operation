function w = OFO_without_integer(Aq,bq,G,f)

x=sdpvar(94,1);
ops = sdpsettings('verbose',0,'cachesolvers',1, 'solver','gurobi','debug',1);
diagnostics = optimize(Aq*x<=bq, x'*G*x+2*f'*x,ops);

FLAG = diagnostics.problem;

if FLAG ~=0 % check whether or not the QP was solved
    FLAG
%     pause
    error('QP of FO controller not solved');
end

w = value(x);

end