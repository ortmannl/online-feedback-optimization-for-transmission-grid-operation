function w = OFO_with_integer(Aq,bq,G,f)

x = sdpvar(84,1);
tap = intvar(10,1);
y = [x;.1/16*tap];

ops = sdpsettings('verbose',0,'cachesolvers',1, 'solver','gurobi','debug',1);
% ops = sdpsettings('verbose',0,'cachesolvers',1, 'solver','mosek','debug',1);
diagnostics = optimize(Aq*y<=bq, y'*G*y+2*f'*y,ops);

FLAG = diagnostics.problem;

if FLAG ~=0 % check whether or not the QP was solved
    FLAG
%     pause
    error('QP of FO controller not solved');
    
end
w = value(y);
end