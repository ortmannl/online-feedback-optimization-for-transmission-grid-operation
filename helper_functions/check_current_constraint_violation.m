function [branch_ids, percentage_overload] = check_current_constraint_violation(blocaux)

% calculate line flow
flow = max( sqrt(blocaux.branches_inside(:,14).^2 + blocaux.branches_inside(:,15).^2), sqrt(blocaux.branches_inside(:,16).^2 + blocaux.branches_inside(:,17).^2));

branch_ids = find(flow>blocaux.branches_inside(:,6) & blocaux.branches_inside(:,6) > 0);

percentage_overload = (flow(branch_ids)-blocaux.branches_inside(branch_ids,6))./blocaux.branches_inside(branch_ids,6)*100;

end

