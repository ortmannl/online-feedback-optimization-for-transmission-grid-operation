%% extract the wind power data from the .txt file

% John Maeght from RTE provided us with active power data of a wind farm

% The active power rises from nearly 0 to 100% within just a few minutes

% This script reads the data from the .txt

% Lukas Ortmann 19.11.2020

% import text file which contains the data
% the data is 0 for no wind  and 1 for full wind
wind_data = importdata('raw_wind_power_data.txt');

% load the french grid
mpc = caseFR7019;
% extract the blocaux area
blocaux_gen = extract_generators(mpc);
% get the installed wind power of the wind farms in the blocaux area
p_blocaux = blocaux_gen(:,2);

% for every data point in the wind .txt file we multiply the
% installed wind power with the percentage of wind that is produced
% in that moment (0 for non, 1 for full)
data = zeros(length(wind_data),length(p_blocaux));
for i = 1:length(wind_data)
   data(i,:) = p_blocaux*wind_data(i); 
end

%% filter the wind power data
% The wind power was quantitized with 1 MW and therefore has large jumps in
% it. We will filter it to make the data series more smooth and therefore
% more realistic

s=tf('s');
tau = 10;
filter_cont = 1/(tau*s+1);
filter_disc = c2d(filter_cont,1);

data_filtered = filtfilt(filter_disc.Numerator{:}, filter_disc.Denominator{:}, data);

% figure
% plot(data)
% hold on
% plot(data_filtered)
%% construct the timeseries

time = 1:1600;

p_changing_wind_power = timeseries(data_filtered,time);

save('p_changing_wind_power','p_changing_wind_power');

