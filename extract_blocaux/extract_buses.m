function bus = extract_buses(mpc)

%% This function extracts the buses in the Blocaux area

% Lukas Ortmann 07.11.19

% extract the bus ids from the text file "blocaux.txt"
bus_ids = extract_bus_ids_from_txt;

% determine the number of buses
number_buses = size(bus_ids);


index_buses = [];                        % initialize an empty arrays to save the rows which have branches including that bus
% go through all buses
for i = 1 : number_buses
    index_buses = [ index_buses ; find( mpc.bus(:,1)==bus_ids(i) ) ];    % find rows in first column of mpc.bus that correspond to the current bus (bus_ids(i))
end

bus = mpc.bus(index_buses,:);

end
