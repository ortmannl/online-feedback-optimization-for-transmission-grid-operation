function [branches_inside, branches_connecting] = extract_branches(mpc)

%% Isolate the lines of the Blocaux area from the txt

% This script takes the Matpower case provided by RTE and extracts the
% branches that are part of the Blocaux area.

% Lukas Ortmann 07.11.19

% extract the bus ids from the text file "blocaux.txt"
buses = extract_bus_ids_from_txt;

% determine the number of buses
number_buses = size(buses);


index_branches = [];                        % initialize an empty arrays to save the rows which have branches including that bus
% go through all buses
for i = 1 : number_buses
    index_branches = [ index_branches ; find( mpc.branch(:,1)==buses(i) ) ];    % find rows in first column of mpc.branch that correspond to the current bus (buses(i))
    index_branches = [ index_branches ; find( mpc.branch(:,2)==buses(i) ) ]; % find rows in second column of mpc.branch that correspond to the current bus (buses(i))
end
index_branches = unique(index_branches);

% get number of branches
number_branches = size(index_branches);
% get number of elements in one branch
number_elements = length(mpc.branch(1,:));
% initialize an array to save the branches
branches = zeros(number_branches(1) , number_elements);

% get the branches
for i = 1 : number_branches
    branches(i,:) = mpc.branch(index_branches(i),:);
end

% initialize two empty arrays to take up the branches inside the blocaux area
% and the branches connecting this area to the rest of the grid
branch_inside2 = [];
branch_connecting = [];
%% seperate branches in branches within the Blocaux area and branches connecting the Blocaux area to the rest of the grid
for i = 1: number_branches
    % are both buses connected through this branch in the area?
    % if yes then inside
    if find(buses == branches(i,1)) & find(buses == branches(i,2))
        branch_inside2(end+1,:) = branches(i,:);
    else % if not they are connecting the area to the rest of the grid
        branch_connecting(end+1,:) = branches(i,:);
    end 
end

%% Make sure for every branch the first bus has the higher number.
% It is easier to go through the branches then

% number_branches_inside = size(branch_inside2);
% for i = 1 : number_branches_inside
%     if branch_inside2(i,1) > branch_inside2(i,2)
%         temp = branch_inside2(i,1);
%         branch_inside2(i,1) = branch_inside2(i,2);
%         branch_inside2(i,2) = temp;
%         branch_inside2(i,14:17) = -branch_inside2(i,14:17); % change the sign of the power flow, because now the power is not flowing from a to b anymore but from b to a
%     end
% end

%% Delete the double branches
% The branches inside the area show up twice because they are found once by looking for the from-bus and a second time when looking at the to-bus

% ATTENTION: This also reduces double lines to a single line
% number_branches_inside = size(branch_inside2);
% branch_inside = [];
% for i = 1 : number_branches_inside
%     temp = branch_inside2(:,1:13)==branch_inside2(i,1:13);
% %     temp = temp(:,1)==1 & temp(:,2)==1;
%     temp = sum(temp,2)==13;
%     temp = find(temp);
%     if temp(1)==i
%        branch_inside(end+1,:) = branch_inside2(i,:);
%     end
% end


%% Output
branches_inside = branch_inside2;
branches_connecting = branch_connecting;

end
