function bus_ids = extract_bus_ids_from_txt()

%% This function extracts the buses in the Blocaux area from the text file

% Lukas Ortmann 07.11.19

% import text file which contains the nodes in the Blocaux area
node_file = importdata('blocaux.txt');

% determine the number of rows in the document
number_rows = size(node_file);

bus_ids = zeros(number_rows(1)-1,1);
% go through all rows starting at the second
for i = 2 : number_rows
    row_content = node_file{i}; % take what is written in row i
    bus_ids(i-1) = str2double(row_content(1:4));
end

end
