function blocaux = extract_blocaux(mpc)

%% Extract Blocaux area from Matpower Case

% get the buses from the .txt file
blocaux.bus = extract_buses(mpc);

[blocaux.branches_inside, blocaux.branches_connecting] = extract_branches(mpc);

blocaux.branch = [blocaux.branches_inside; blocaux.branches_connecting];

blocaux.gen = extract_generators(mpc);

blocaux.baseMVA = 100;

end