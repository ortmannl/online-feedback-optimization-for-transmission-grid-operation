function generators = extract_generators(mpc)

%% This function extracts the generators in the Blocaux area

% Lukas Ortmann 09.12.19

% extract the bus ids from the text file "blocaux.txt"
bus_ids = extract_bus_ids_from_txt;

% determine the number of buses
number_buses = size(bus_ids);


index_gens = [];                        % initialize an empty arrays to save the indices of the generators connected to buses in the Blocaux area
% go through all buses
for i = 1 : number_buses
    index_gens = [ index_gens ; find( mpc.gen(:,1)==bus_ids(i) ) ];    % find rows in first column of mpc.bus that correspond to the current bus (bus_ids(i))
end

generators = mpc.gen(index_gens,:);

end
