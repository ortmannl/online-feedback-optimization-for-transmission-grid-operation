function [vm_out, va_out, pg_out, qg_out, pd_out,qd_out, pf_out,qf_out, pt_out,qt_out, success] ...
    = extrinsic_ACPF(vm_in, va_in, pg_in, qg_in, pd_in, qd_in, blocaux_tap_changer_positions)
%% This function is a wrapper function for MATPOWER's runpf so that it can
%% be used in Simulink. It also implements the analysis of OLTCs in steady
%% state. 
% Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020
    define_constants;
    %% Get mpm struct from base workspace
    mpm = evalin('base','mpm');
    idx_branches_in_oltc = evalin('base','idx_branches_in_oltc');
    index_blocaux_OLTCs_in_branch = evalin('base','index_blocaux_OLTCs_in_branch'); 
    %% Read values from Simulink
    mpm.mpc.bus(:,PD) = pd_in; %In MW
    mpm.mpc.bus(:,QD) = qd_in; %In MVAr
    mpm.mpc.bus(:,VM) = vm_in; %In p.u.
    mpm.mpc.bus(:,VA) = va_in; %In degrees
    mpm.mpc.gen(:,PG) = pg_in; %In MW
    mpm.mpc.gen(:,QG) = qg_in; %In MVAr
    
    
    %% Set Blocaux Tap Changers
    
    mpm.mpc.branch(index_blocaux_OLTCs_in_branch,TAP) = blocaux_tap_changer_positions;
    
    %% Solve power flow
    % standard matpower solver
    if ~mpm.oltc.enable
        [mpm.mpc, success] = runpf(mpm.mpc,mpm.mpopt);
    % standard matpower solver & use OLTCs (in Blocaux area)
    else
        [mpm, success] = OLTC(mpm,idx_branches_in_oltc);
    end

    %% Write values back to Simulink
    pg_out = mpm.mpc.gen(:,PG); %In MW
    qg_out = mpm.mpc.gen(:,QG); %In MVAr
    vm_out = mpm.mpc.bus(:,VM); %In p.u
    va_out = mpm.mpc.bus(:,VA); %In degrees
    pd_out = mpm.mpc.bus(:,PD); %In MW
    qd_out = mpm.mpc.bus(:,QD); %In MVAr
    pf_out = mpm.mpc.branch(:,PF); %In MW
    qf_out = mpm.mpc.branch(:,QF); %In MVAr
    pt_out = mpm.mpc.branch(:,PT); %In MW
    qt_out = mpm.mpc.branch(:,QT); %In MVAr
    %% Store mpm struct in base workspace
    assignin('base','mpm',mpm);
end