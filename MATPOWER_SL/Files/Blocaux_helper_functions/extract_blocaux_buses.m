function bus = extract_blocaux_buses(mpc, bus_ids)

%% This function extracts the buses in the Blocaux area
% Lukas Ortmann 07.11.19, 
% Adapted by Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020

% Determine the number of buses
number_buses = size(bus_ids, 1);

% Initialise an empty array to save the rows which have branches including 
% that bus
index_buses = zeros(number_buses,1);                        
% Go through all buses
for i = 1 : number_buses
    % Find rows in first column of mpc.bus that correspond to the current 
    % bus (bus_ids(i))
    index_buses(i,:) = find( mpc.bus(:,1)==bus_ids(i) );    
end

bus = mpc.bus(index_buses,:);

end
