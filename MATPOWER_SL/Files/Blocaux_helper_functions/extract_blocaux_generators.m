function [generators, index_gens] = extract_blocaux_generators(mpc, bus_ids)

%% This function extracts the generators in the Blocaux area and returns
%% their indices in mpc.gen

% Lukas Ortmann 09.12.19
% Adapted by Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020

% Determine the number of buses
number_buses = size(bus_ids);

 % Initialise an empty array to save the indices of the generators 
 % connected to buses in the Blocaux area
index_gens = [];          

% Go through all buses
for i = 1 : number_buses
    % find rows in first column of mpc.bus that correspond to the current 
    % bus (bus_ids(i))
    index_gens = [ index_gens ; find( mpc.gen(:,1)==bus_ids(i) ) ];    
end

generators = mpc.gen(index_gens,:);

end
