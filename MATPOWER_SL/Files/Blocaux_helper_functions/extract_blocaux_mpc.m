function blocaux = extract_blocaux_mpc(mpc)

%% Extract Blocaux area from Matpower Case

% get the buses from the .txt file
blocaux.bus = extract_blocaux_buses(mpc);

[blocaux.branches_inside, blocaux.branches_connecting] = extract_blocaux_branches(mpc);

blocaux.gen = extract_blocaux_generators(mpc);

end