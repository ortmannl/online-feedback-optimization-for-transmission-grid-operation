function [idx_branches_inside, idx_branches_connecting] = extract_blocaux_branch_idx(mpc)
%% This function takes the Matpower case provided by RTE and extracts the
%% indices of mpc.branches of the branches that are part of the Blocaux area.
% Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020

% Extract the bus ids from the text file "blocaux.txt"
buses = extract_bus_ids_from_txt;

index_branches_from = [];
index_branches_to = [];
% Go through all buses
for i = 1 : size(buses)
    % Find rows in first column of mpc.branch that correspond to the 
    % current bus (buses(i))
    index_branches_from =[index_branches_from ;...
        find( mpc.branch(:,1)==buses(i) ) ];
    % Find rows in second column of mpc.branch that correspond to the 
    % current bus (buses(i))
    index_branches_to =[index_branches_to ;...
        find( mpc.branch(:,2)==buses(i) ) ]; 
end
% Find branch indices of branches completely inside Blocaux area
idx_branches_inside = intersect(index_branches_from, index_branches_to);
% Find branch indices of branches connected to Blocaux area on one end
idx_branches_connecting = setxor(index_branches_from, index_branches_to);

end
