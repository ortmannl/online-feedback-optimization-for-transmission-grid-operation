%% Initialisation of the Simulink simulation

% Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020

%% Load input data
load('q_ORPF');
load('p_changing_wind_power');


%% From MATPOWER: Defines all the field names within the MATPOWER case struct
define_constants;

%% Load MATPOWER case from file
mpm.mpc = loadcase(caseFR7019);

%% Parametrize the case file
mpm.mpc = parametrize_case_file(mpm.mpc);

%% Parametrize the Blocaux area
mpm.mpc = parametrize_blocaux(mpm.mpc, p_changing_wind_power);

%% Configure MATPOWER options to reduce console clutter
mpm.mpopt = mpoption(mpoption, 'verbose', 0); % avoid printing during iteration
mpm.mpopt.out.all=0; % avoid printing reports after simulations
mpm.mpopt.pf.tool=10^-12;

%% Enforce reactive power constraints
mpm.mpopt.pf.enforce_q_lims = 1;

%% presolve the grid once
mpm.mpc = runpf(mpm.mpc,mpm.mpopt);

%% Set up OLTC configuration
mpm.oltc.enable = 0;
mpm.oltc.V_deadband = 0.015;
mpm.oltc.TAP_stepsize = 0.00625;
mpm.oltc.verbose = false;
mpm.oltc.max_iter = 100;

%% Define Sizes of Blocaux buses, branches, and generators
% Necessary to allocate memory later
zeros_num_branches = zeros(size(mpm.mpc.branch(:,1)));
blocaux_number_buses = size(importdata('blocaux.txt'),1)-1;
[branches_inside, branches_connecting] = extract_blocaux_branches(mpm.mpc);
blocaux_number_branches_inside = size(branches_inside,1);
blocaux_number_branches_connecting = size(branches_connecting,1);
[blocaux_generators,index_blocaux_gens] = extract_blocaux_generators(mpm.mpc,extract_bus_ids_from_txt());
blocaux_number_generators = size(index_blocaux_gens,1);
blocaux_buses = extract_buses(mpm.mpc);
index_blocaux_buses = blocaux_buses(:,1);
%% Define Inputs for Simulation
std_load = mpm.mpc.bus(:,PD) + 1j*mpm.mpc.bus(:,QD); %In MVA
std_gen = mpm.mpc.gen(:,PG) + 1j*mpm.mpc.gen(:,QG); %In MVA


%% Extract the Blocaux area
blocaux = extract_blocaux(mpm.mpc);

%% Get the indexes of the branches that are transformers with OLTCs
        
%         % Get branch data for branches with OLTCs within Blocaux area
%         [idx_branches_in, idx_branches_connecting] = extract_blocaux_branch_idx(mpm.mpc);
%         idx_branches_in_oltc = ...
%             idx_branches_in( mpm.mpc.branch(idx_branches_in,TAP)~=1 ...
%             & mpm.mpc.branch(idx_branches_in,TAP)~=0 );
%       
        % Get the indexes of all OLTCs and not just the Blocaux area
        idx_branches_all_tap_changers = find_OLTC_indexes(mpm.mpc);
        index_blocaux_OLTCs_in_all_OTLCs = [4, 139, 140, 141, 166, 167, 774, 775, 5, 6]; % all
        index_blocaux_OLTCs_in_branch = idx_branches_all_tap_changers(index_blocaux_OLTCs_in_all_OTLCs);
        % delete the indexes of these blocaux tap changers because they are
        % going to be controlled by the controller now
        idx_branches_in_oltc = idx_branches_all_tap_changers;
        idx_branches_in_oltc(index_blocaux_OLTCs_in_all_OTLCs)=[];
        nmb_idx_branches_in_oltc = length(idx_branches_in_oltc);
        % get the indexes of the OLTCs in the blocaux area
        
%         idx_branches_in_oltc_blocaux = find_OLTC_indexes(blocaux);
        idx_branches_in_oltc_blocaux = [33;35;36;37;38;39;40;41;54;55];
%         nmb_idx_branches_in_oltc_blocaux = length(idx_branches_in_oltc_blocaux);
        nmb_idx_branches_in_oltc_blocaux=10;

%% Get the struct with all kinds of indexes
indexes = index_helper(mpm.mpc);