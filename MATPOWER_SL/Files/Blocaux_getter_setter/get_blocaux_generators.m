function [gen_ids, b_pg_out, b_qg_out] = get_blocaux_generators()
%% This function extracts the Blocaux generator data from mpm
% gen_ids are the bus ids of the buses with generators connected

% Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020
	%% Get bus_ids
    bus_ids = extract_bus_ids_from_txt;
    
    %% Get mpm object
    mpm = evalin('base','mpm');    
    define_constants;
    
    %% Extract Blocaux generator data from mpm object
    [blocaux_generators_out, gen_ids] = extract_blocaux_generators(mpm.mpc, bus_ids);
    b_pg_out = blocaux_generators_out(:,PG);
    b_qg_out = blocaux_generators_out(:,QG);
end