function [losses_all, losses_blocaux] = get_losses_all_blocaux()
%% This function extracts the Blocaux losses from mpm

% Lukas Ortmann 23-07.2021
    
    %% Get mpm object
    mpm = evalin('base','mpm');  
    indexes = evalin('base','indexes');
    
    %% Extract Blocaux losses from mpm object
    all_losses = get_losses(mpm.mpc); % Matpower function that returns the losses in the whole grid
    losses_all = real(sum(all_losses));
    losses_blocaux = real(sum(all_losses(indexes.blocaux_branches))); % take only the losses on the branches inside the Blocaux area, sum them up and only take the active power losses and not the reactive "losses"
end