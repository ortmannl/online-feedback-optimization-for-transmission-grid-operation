function [tap_ratio] = get_blocaux_tap_ratios()
%% Get the blocaux tap ratios
% Lukas Ortmann on 04-01.2021
    
    %% Get mpm object
    mpm = evalin('base','mpm');    
    %% Get OLTC indexes
    idx_branches_in_oltc_blocaux = evalin('base','idx_branches_in_oltc_blocaux');
    
    %% Extract the tap ratios
    blocaux = extract_blocaux(mpm.mpc);
    tap_ratio = blocaux.branch(idx_branches_in_oltc_blocaux,9);
end