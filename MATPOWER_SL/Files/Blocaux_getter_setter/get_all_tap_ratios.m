function [tap_ratio] = get_all_tap_ratios()
%% Get all the tap ratios
% Lukas Ortmann on 23-11.2020
    
    %% Get mpm object
    mpm = evalin('base','mpm');    
    %% Get OLTC indexes
    idx_branches_in_oltc = evalin('base','idx_branches_in_oltc');
    
    %% Extract the tap ratios
    tap_ratio = mpm.mpc.branch(idx_branches_in_oltc,9);
end