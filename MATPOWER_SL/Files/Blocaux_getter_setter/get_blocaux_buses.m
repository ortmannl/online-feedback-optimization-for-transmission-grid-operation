function [bus_ids, b_vm_out, b_va_out, b_pd_out, b_qd_out] ...
    = get_blocaux_buses()
%% This function extracts the Blocaux bus data from mpm
% Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020

	%% Get bus_ids
    bus_ids = extract_bus_ids_from_txt;
    
    %% Get mpm object
    mpm = evalin('base','mpm');    
    define_constants;
    %% Extract Blocaux bus data from mpm object
    blocaux_buses_out = extract_blocaux_buses(mpm.mpc, bus_ids);
    b_pd_out = blocaux_buses_out(:,PD);
    b_qd_out = blocaux_buses_out(:,QD);
    b_vm_out = blocaux_buses_out(:,VM);
    b_va_out = blocaux_buses_out(:,VA);
end