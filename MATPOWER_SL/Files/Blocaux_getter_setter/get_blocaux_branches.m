function [bus_ids_from_inside, bus_ids_to_inside, ...
    b_pf_inside, b_qf_inside, b_pt_inside, b_qt_inside, ...
    bus_ids_from_connecting, bus_ids_to_connecting, ...
    b_pf_connecting, b_qf_connecting, b_pt_connecting, b_qt_connecting] ...
    =get_blocaux_branches()
%% This function extracts the branch data from mpm.mpc for branches inside
%% and connecting to the Blocaux area
% Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020

    %% Get mpm object
    mpm = evalin('base','mpm');
    define_constants;
    
    %% Get indices of Blocaux branches in mpm.mpc.branches
    [idx_branches_in, idx_branches_conn] = extract_blocaux_branch_idx(mpm.mpc);

    %% Extract Blocaux branch data from mpm object
    bus_ids_from_inside = mpm.mpc.branch(idx_branches_in,F_BUS);
    bus_ids_to_inside = mpm.mpc.branch(idx_branches_in,T_BUS);
    
    b_pf_inside = mpm.mpc.branch(idx_branches_in,PF);
    b_qf_inside = mpm.mpc.branch(idx_branches_in,QF);
    b_pt_inside = mpm.mpc.branch(idx_branches_in,PT);
    b_qt_inside = mpm.mpc.branch(idx_branches_in,QT);
    
    bus_ids_from_connecting = mpm.mpc.branch(idx_branches_conn,F_BUS);
    bus_ids_to_connecting = mpm.mpc.branch(idx_branches_conn,T_BUS);
    
    b_pf_connecting = mpm.mpc.branch(idx_branches_conn,PF);
    b_qf_connecting = mpm.mpc.branch(idx_branches_conn,QF);
    b_pt_connecting = mpm.mpc.branch(idx_branches_conn,PT);
    b_qt_connecting = mpm.mpc.branch(idx_branches_conn,QT);
    
end

