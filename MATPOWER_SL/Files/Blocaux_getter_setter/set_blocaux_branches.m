function [pf_out, qf_out, pt_out,qt_out] ...
    =  set_blocaux_branches(pf_in, qf_in, pt_in, qt_in, ...
    b_pf_inside, b_qf_inside, b_pt_inside, b_qt_inside, ...
    b_pf_connecting, b_qf_connecting, b_pt_connecting, b_qt_connecting)
%% This function allows the Blocaux branches to be set separately
    % - All values are written into the mpm.bus matrix and overwritten
    %   with the Blocaux buses (this is necessary due to indexing)
    % - Since the input vectors do not have indices, they must be sorted
    
    % Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020
    %% Get mpm object
    mpm = evalin('base','mpm');    
    define_constants;

    %% Read global values from Simulink, write them into mpm
    mpm.mpc.branch(:,PF) = pf_in; %In MW
    mpm.mpc.branch(:,QF) = qf_in; %In MVAr
    mpm.mpc.branch(:,PT) = pt_in; %In MW
    mpm.mpc.branch(:,QT) = qt_in; %In MVAr
    
    %% Get the indices of branches inside Blocaux area
    [idx_inside, idx_connecting] = extract_blocaux_branch_idx(mpm.mpc);

    %% Overwrite values for inside branches
    mpm.mpc.branch(idx_inside, PF) = b_pf_inside;
    mpm.mpc.branch(idx_inside, QF) = b_qf_inside;
    mpm.mpc.branch(idx_inside, PT) = b_pt_inside;
    mpm.mpc.branch(idx_inside, QT) = b_qt_inside;
    
    %% Overwrite values for connecting branches
    mpm.mpc.branch(idx_connecting, PF) = b_pf_connecting;
    mpm.mpc.branch(idx_connecting, QF) = b_qf_connecting;
    mpm.mpc.branch(idx_connecting, PT) = b_pt_connecting;
    mpm.mpc.branch(idx_connecting, QT) = b_qt_connecting;
    
    %% Write values back to Simulink
    pf_out = mpm.mpc.branch(:,PF); %In MW
    qf_out = mpm.mpc.branch(:,QF); %In MVAr
    pt_out = mpm.mpc.branch(:,PT); %In MW
    qt_out = mpm.mpc.branch(:,QT); %In MVAr
    
    %% Store mpm struct in base workspace
    assignin('base','mpm',mpm);
end

