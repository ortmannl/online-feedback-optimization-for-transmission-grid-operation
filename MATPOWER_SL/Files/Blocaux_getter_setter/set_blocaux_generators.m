function [pg_out, qg_out]...
    = set_blocaux_generators(pg_in, qg_in, b_pg_in, b_qg_in, index_blocaux_gens)
    %% This function allows the Blocaux buses to be set separately
    % - All values are written into the mpm.bus matrix and overwritten
    %   with the Blocaux buses (this is necessary due to indexing)
    % - Since the input vectors do not have indices, they must be sorted
    
    % Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020
    %%
    define_constants;
    %% Get mpm object
    mpm = evalin('base','mpm');
 
    %% Read global values from Simulink, write them into mpm
    mpm.mpc.gen(:,PG) = pg_in; %In MW
    mpm.mpc.gen(:,QG) = qg_in; %In MVAr
    
    %% Overwrite with Blocaux generators; need to be sorted by index
    mpm.mpc.gen(index_blocaux_gens,PG) =  b_pg_in;
    mpm.mpc.gen(index_blocaux_gens,QG) =  b_qg_in;

    %% Write values back to Simulink
    pg_out = mpm.mpc.gen(:,PG); %In MW
    qg_out = mpm.mpc.gen(:,QG); %In MVAr

    %% Store mpm struct in base workspace
    assignin('base','mpm',mpm);
end