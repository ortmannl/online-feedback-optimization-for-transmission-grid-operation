function [vm_out, va_out, pd_out,qd_out] ...
= set_blocaux_buses(vm_in, va_in, pd_in, qd_in, ...
       b_vm_in, b_va_in, b_pd_in, b_qd_in)
    %% This function allows the Blocaux buses to be set separately
    % - All values are written into the mpm.bus matrix and overwritten
    %   with the Blocaux buses (this is necessary due to indexing)
    % - Since the input vectors do not have indices, they must be sorted
    
    % Roger Germann, Kerry Jansen, Denis Mikhaylov 02-05.2020
    
    %% Get mpm object
    mpm = evalin('base','mpm'); 
    
    %% Read global values from Simulink, write them into mpm
    mpm.mpc.bus(:,PD) = pd_in; %In MW
    mpm.mpc.bus(:,QD) = qd_in; %In MVAr
    mpm.mpc.bus(:,VM) = vm_in./mpm.mpc.bus(:,BASE_KV);  %Convert from kV to p.u  
    mpm.mpc.bus(:,VA) = va_in; %In degrees

    %% Get the indices, where the Blocaux buses are (external indexing)
    index_buses = zeros(size(b_vm_in));
    bus_ids = extract_bus_ids_from_txt();
    number_buses = size(bus_ids);
    for i = 1 : number_buses
        index_buses(i) = find( mpc.bus(:,1)==bus_ids(i) );    % find rows in first column of mpc.bus that correspond to the current bus (bus_ids(i))
    end
    
    %% Overwrite with Blocaux buses; need to be sorted by index
    mpm.mpc.bus(index_buses,PD) = b_pd_in;
    mpm.mpc.bus(index_buses,QD) = b_qd_in;
    mpm.mpc.bus(index_buses,VM) = b_vm_in;
    mpm.mpc.bus(index_buses,VA) = b_va_in;
 

    %% Write values back to Simulink
    vm_out = mpm.mpc.bus(:,VM).*mpm.mpc.bus(:,BASE_KV); %Convert back from p.u to kV
    va_out = mpm.mpc.bus(:,VA); %In degrees
    pd_out = mpm.mpc.bus(:,PD); %In MW
    qd_out = mpm.mpc.bus(:,QD); %In MVAr
    
    %% Store mpm struct in base workspace
    assignin('base','mpm',mpm);
end