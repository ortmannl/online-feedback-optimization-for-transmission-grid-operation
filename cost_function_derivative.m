function nabla_Phi = cost_function_derivative(q,controller_settings)

sensitivity_vp = controller_settings.sensitivity_vp;
sensitivity_vq = controller_settings.sensitivity_vq;
sensitivity_vtap = controller_settings.sensitivity_vtap;
sensitivity_ap = controller_settings.sensitivity_ap;
sensitivity_aq = controller_settings.sensitivity_aq;
sensitivity_atap = controller_settings.sensitivity_atap;
sensitivity_losses = controller_settings.sensitivity_losses;
weight_q = controller_settings.weight_q;




 if controller_settings.perfect_sensitivity == 1
    sensitivity_losses_q = controller_settings.sensitivity_losses_q;
    sensitivity_losses_p = controller_settings.sensitivity_losses_p;
    sensitivity_losses_tap = controller_settings.sensitivity_losses_tap;
    
    nabla_q_Phi = sensitivity_losses_q;
    nabla_p_Phi = sensitivity_losses_p -1;
    nabla_tap_Phi = sensitivity_losses_tap;
else
    %     nabla_q_Phi = (sensitivity_vq' * sensitivity_losses.vm' + sensitivity_aq' * sensitivity_losses.va');
    nabla_q_Phi = 2 * weight_q * q;
    nabla_p_Phi = 0*(sensitivity_vp' * sensitivity_losses.vm' + sensitivity_ap' * sensitivity_losses.va') -1;
    nabla_tap_Phi = sensitivity_vtap' * sensitivity_losses.vm' + sensitivity_atap' * sensitivity_losses.va';
end
nabla_Phi = [nabla_q_Phi; nabla_p_Phi; nabla_tap_Phi];

end

